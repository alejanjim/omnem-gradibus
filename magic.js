/*
 * Copyright (c) <2016> <Manuel Alejandro Jiménez Quintero>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 */

"use strict";

/**
 * Returns the URL of the full article.
 * @param {object} Property redirectURL.
 * @return {string} The new URL.
 */
function redirect(requestDetails) {
	if (requestDetails.url.indexOf("ALLSTEPS") == -1) {
		var finalUrl = requestDetails.url.split("/");
		return {
			redirectUrl: finalUrl[0] + "//www.instructables.com/id/" + finalUrl[4] + "/?ALLSTEPS"
		};
	}
}

/**
 * Add redirect as a listener to onBeforeRequest, only for the article URL.
 * Make it "blocking" so we can modify the URL.
 */
chrome.webRequest.onBeforeRequest.addListener(
	redirect,
	{
		urls: ["*://www.instructables.com/id/*/", "*://www.instructables.com/id/*/*"]
	},
	["blocking"]
);
