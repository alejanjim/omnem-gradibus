# Español

## Sobre el contenido y la licencia

### Resumen

Este complemento muestra de forma automática todos los pasos de un artículo de Instructables en una sola página.

Por lo tanto, ya no será necesario hacer clic en el botón "View All Steps" en cada artículo que cargue (quizás con uno no haya problema, pero cuando se trata de muchos...). Realmente creo que de esta manera se consigue una mejor experiencia al navegar en este gran sitio.

### Derechos de propiedad intelectual

Copyright © 2016 Manuel Alejandro Jiménez Quintero

### Información sobre la licencia

#### Acuerdo de licencia

Aviso a todos los usuarios: lea detenidamente los acuerdos legales usados en este proyecto, los mismos que establecen los términos y condiciones generales de uso y distribución del software para el que se concede la licencia. Tales acuerdos se incluyen en este documento bajo el nombre de ANEXO I.

Si no está de acuerdo con todas las condiciones descritas anteriormente, NO INSTALE NI UTILICE EL SOFTWARE.

Este programa es Software Libre; Ud. puede redistribuirlo y/o modificarlo bajo los términos de la Licencia Pública General GNU tal como está publicada por la Free Software Foundation; ya sea la versión 3 de la licencia, o (según su elección) cualquier otra versión posterior. Este programa se distribuye con la esperanza de que sea útil, pero SIN NINGUNA GARANTÍA; incluso sin las garantías de COMERCIALIZACIÓN o USABILIDAD O UTILIDAD PARA USOS PARTICULARES. Lea la Licencia Pública General GNU para más detalles. Ud. debería haber recibido una copia de dicha licencia junto con este programa; si no fue así, escriba a la Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## Anexo I

En el siguiente enlace es posible encontrar una copia de la Licencia Pública General GNU en su versión original en inglés:

[GNU General Public License](http://www.gnu.org/licenses/gpl-3.0.txt)

# English

## Content & Licence

### Summary

This add-on automatically displays all steps in one page for each article in Instructables.

So, you'll don't have to do click on "View All Steps" button every time you open an Instructables article (with just one tab open do a click in the button isn't a heavy work but with many tabs open...). I really think that with this manner you'll get a more enjoyable experience in this great site.

### Intellectual property rights

Copyright © 2016 Manuel Alejandro Jiménez Quintero

### Terms and conditions of use

#### Licence agreement

To all users: By downloading, installing, copying, accessing or using this software, you agree to the terms and conditions of the GNU General Public License (Attachment I).

If you are not agree with these terms, please NOT USE THIS SOFTWARE.

This program is Free Software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

## Attachment I

In the following link you can find a full copy of the GNU General Public License:

[GNU General Public License](http://www.gnu.org/licenses/gpl-3.0.txt)
